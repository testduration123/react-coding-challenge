# React Coding Challenge

## Overview

Building upon this skeleton project, implement a Monthly Calendar UI for the current month using React.js and Redux. The solution should implement as many of the criteria points outlined below. The solution should use mock/local data only, there should be no API involvement. 

## Getting Started

 - Clone this repository
 - Install dependencies `yarn install`
 - Run the development server `yarn start`

## Criteria

 - As a user, I should be displayed with a Monthly Calendar of the current month
 - As a user, I should be able to add a reminder on a selected day
	 - User can enter a description (*max 25 characters*) **and** specify a time.
	 - User can select a colour for the reminder
- As a user, I should see my reminders for a given day ordered by time. 
- As a user, I should see a reminder displayed in the colour that I selected.
- As a user, I should be able to edit a existing reminder
- As a user, I should be able to delete a reminder

## Things to think about

- Re-usable components
- Handling the overflow of reminders on a given day (scroll?) 
- Redux structure for storing the calendar data/reminders. 
 - Overall UX for creating a reminder ([Dribble](https://dribbble.com/search?q=calendar) or [Codepen](https://codepen.io/) for inspiration)

## Example UI

You have full creative freedom with the design of the solution, but we have provided an example Monthly Calendar display below for your inspiration. 

*The design of the final solution is considered **less important** than the full coverage of the above criteria.*

![enter image description here](https://developerjobsboard.co.uk/wp-content/uploads/2018/07/Screen-Shot-2018-07-25-at-14.21.00-768x549.png)

## Submitting the Solution

Your solution should be submitted by raising a Pull Request against this repository **before** the communicated deadline.  

