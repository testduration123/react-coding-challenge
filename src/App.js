import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app">
        <div id="replace-me">
            <span>Delivery Blueprints - <b>React Coding Challenge</b></span><br />
            <span>Requirements - <a href="https://gitlab.com/deliveryblueprintspublic/react-coding-challenge">https://gitlab.com/deliveryblueprintspublic/react-coding-challenge</a></span><br />
            <span>Good Luck!</span>
        </div>
      </div>
    );
  }
}

export default App;
